FROM jenkins:latest


USER root
RUN apt-get update
RUN apt-get install -yy \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common


RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -


RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"


RUN apt-get update


RUN apt-get install -yy docker-ce


RUN rm -rf /var/lib/apt/lists/*
RUN echo "jenkins ALL=NOPASSWD: ALL" >> /etc/sudoers


USER jenkins